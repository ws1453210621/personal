
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.Document;

import org.jsoup.Jsoup;
public class uml {




	public static void main(String[] args) throws IOException {
		//设置变量放两个网页地址
		File smallHtml = new File("small.html","UTF-8");
		File allHtml = new File("all.html","UTF-8");
		// 使用InPutStream流读取properties文件
		Properties properties = new Properties();
		properties.load(new FileInputStream("total.properties"));
		// 配置文件取出元素
		double SumBrfore = (double) Integer.parseInt(properties.getProperty("before"));
		// 算出课前测试的总共的分数
		double SumBase= (double) Integer.parseInt(properties.getProperty("base"));
		// 算出课堂完成部分总共的分数
		double SumTest = (double) Integer.parseInt(properties.getProperty("test"));
		// 算出小测部分的总共的分数
		double Sumprogram = (double) Integer.parseInt(properties.getProperty("program"));
		// 算出编程题部分总共的分数
		double SumAdd = (double) Integer.parseInt(properties.getProperty("add"));
		HashMap<String, Integer> smallpart = getScores(smallHtml);
		HashMap<String, Integer> allpart = getScores(allHtml);				 
		double finalbe=(smallpart.get("MyBefore")+allpart.get("MyBefore"))*0.25;
	    double finalba=(smallpart.get("MyBase")+allpart.get("MBase"))*0.3*0.95;
	    double finalts=(smallpart.get("MyTest")+allpart.get("MyTest"))*0.2;
	    double finalpg=(smallpart.get("MyProgram")+allpart.get("MyProgram"))*0.1;
	    double finalad =(smallpart.get("MyAdd")+allpart.get("MyAdd"))*0.05;
	    double sum=finalbe+finalba+finalts+finalpg+finalad;
		System.out.println(sum);
	}
	//获得云班课中获得的经验值
	private static HashMap<String, Integer> getScores(File file) throws IOException {
		// TODO Auto-generated method stub
		int MyBefore = 0;
		int MyBase = 0;
		int MyTest = 0;
		int MyProgram = 0;
		int MyAdd = 0;
		String name1 = "课堂自测";
		String name2 = "课堂完成部分";
		String name3 = "课堂小测";
		String name4 = "编程题";
		String name5 = "附加题";
		String exp_regex = "\\d+";
		int exp = 0;
		org.jsoup.nodes.Document document = Jsoup.parse(file, "utf-8");
		org.jsoup.select.Elements rootRow = document.getElementsByClass("interaction-row");
		for (int i = 0; i < rootRow.size(); i++) {			
			Elements rowChild = rootRow.get(i).children();
			Elements rowChild2 = rowChild.get(1).children();	
			Elements rowChild2_1 = rowChild2.get(0).children();
			Element section = rowChild2_1.get(1);
			Elements rowChild2_3 = rowChild2.get(2).children();
			Elements rowChild2_3_1 = rowChild2_3.get(0).children();
			Element span7 = rowChild2_3_1.get(rowChild2_3_1.size() - 2);
			// 已参与
			org.jsoup.nodes.Element span8 = rowChild2_3_1.get(rowChild2_3_1.size() - 1);
			// 经验值
			Pattern p = Pattern.compile(exp_regex);
			Matcher matcher = p.matcher(span8.text());
			boolean result = matcher.find();
			if (result) {
				exp = Integer.parseInt(matcher.group(0));
			}
			if (section.text().contains(name1)) {
				MyBefore += exp;
			} else if (section.text().contains(name2)) {
				MyBase += exp;
			} else if (section.text().contains(name3)) {
				MyTest += exp;
// 筛选互评分部分
                 if (child.toString().contains("+")) {
								Scanner sc2 = new Scanner(child.select("span").get(11).text());
								temp = sc2.nextLine().toString().charAt(3) - 48;
								MyTest = MyTest + temp;
			} else if (section.text().contains(name4)) {
				MyProgram += exp;
			} else if (section.text().contains(name5)) {
				MyAdd += exp;
			}
		}

		HashMap<String, Integer> sectionExperience = new HashMap<>();
		sectionExperience.put("MyBefore", MyBefore);
		sectionExperience.put("MyBase", MyBase);
		sectionExperience.put("MyTest", MyTest);
		sectionExperience.put("MyProgram", MyProgram);
		sectionExperience.put("MyAdd", MyAdd);

		return sectionExperience;
	}
}